<?php

header('Content-Type: application/json');
$result = 0 ;
//require_once 'lib/swift_required.php';
$name = $_POST['your-name'];
$tel = $_POST['your-tel'];
$secret="6Lc9WhEUAAAAABMxRjPeII4JHtiat106IMlmWeYT";
$response=$_POST["g-recaptcha-response"];
$verify=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret={$secret}&response={$response}");
$captcha_success=json_decode($verify);
$messages = array (
    'missing-input-secret' =>	'Секретный параметр отсутствует.',
    'invalid-input-secret' => 'Секретный параметр недействителен или имеет неправильный формат.',
    'missing-input-response' =>	'Неверно введена капча.',
    'invalid-input-response' => 'Параметр ответа является недействительным или неправильный формат.'
);

$error = "error-codes";
if ($captcha_success->success != true) {
    $error = $captcha_success->$error;
    $string_message = array('message' => $messages[$error[0]],'status' => 0 );
    $json_string = json_encode($string_message);
    echo $json_string;

    die();
}



// Create the mail transport configuration
//$transport = Swift_MailTransport::newInstance();


// Create the message
//$message = Swift_Message::newInstance();
//$message->setTo(array(
//    "sales@anthracite.com.ua" => "sales@anthracite.com.ua"
//));
//$message->setSubject("Сообщение от пользователя - $name");
//$message->setBody("Номер телефона пользователя $name : $tel");
// Send the email
$name = trim($name);
$tel = trim($tel);
if(!empty($name) && !empty($tel)) {
    //$mailer = Swift_Mailer::newInstance($transport);
    // To use the ArrayLogger
    //$logger = new Swift_Plugins_Loggers_ArrayLogger();
    //$mailer->registerPlugin(new Swift_Plugins_LoggerPlugin($logger));
    $result = mail("sales@anthracite.com.ua", "Anthracite.com.ua Заявка от пользователя - $name", "Имя : $name\nНомер телефона  : $tel");
    //$result = $mailer->send($message, $failures);
    if ($result){
        $string_message = array('message' => "Сообщение отправлено!",'status' => 1 );
    }
    else {
        $string_message = array('message' => "Ошибка при отправке! Повторите попытку позже.",'status' => 0);
    }
}else{
    $string_message = array('message' => "Обязательные поле для заполнения : Имя и Телефон",'status' => 0 );
}

$json_string = json_encode($string_message);
echo $json_string;

