

    $(document).ready(function() {

        $("head").append('<link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext" rel="stylesheet">');
        /*---- SELECT CUSTOM StyLE START -----*/
        $(function() {

            $('select').styler();

        });

        /*---- SELECT CUSTOM StyLE END -----*/

        /*---- POPUP PLUGIN START -----*/

        $(".fancybox").fancybox({
            openEffect	: 'none',
            closeEffect	: 'none'
        });

        /*---- POPUP PLUGIN END -----*/



        /*---- POPUP CUSTOM START -----*/

        var overlay = $('#overlay');
        var open_modal = $('.open_modal');
        var close = $('.modal_close, #overlay');
        var modal = $('.modal_div');

        open_modal.click( function(event){
            event.preventDefault();
            var div = $(this).attr('href');
            overlay.fadeIn(400,
                function(){
                    $(div)
                        .css('display', 'block')
                        .animate({opacity: 1}, 200);
                });
        });

        close.click( function(){
            modal
                .animate({opacity: 0}, 200,
                    function(){
                        $(this).css('display', 'none');
                        overlay.fadeOut(400);
                    }
                );
            modal.find("input").val("");
            clearCaptcha();
        });

        /*---- POPUP CUSTOM END -----*/



    });

    





