function clearCaptcha() {
    grecaptcha.reset(0);
    grecaptcha.reset(1);
}

    $('form').submit(function (e) {
        e.preventDefault();
        var form = $(e.target);
        var button = form.find('button');
        var data = form.serializeArray();
        $.ajax({
            type: "POST",
            url: "./lib/email.php",
            data: data,
            success: function(response)
            {
                if( !response.status ) {
                    $.notify(response.message,"error");
                }
                else {
                    $( "#modal1" ).hide();
                    $( "#overlay" ).hide();
                    form.find("input").val("");
                    $.notify(response.message);

                }
            },
            complete: function () {
                clearCaptcha();

            }
        });
    });
